package guy.droid.im.aesandroid;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by admin on 7/14/2017.
 */

public class Passer implements Parcelable {
    int id;
    String name;
    List<String> skills;

    public Passer() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeStringList(this.skills);
    }

    protected Passer(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.skills = in.createStringArrayList();
    }

    public static final Creator<Passer> CREATOR = new Creator<Passer>() {
        @Override
        public Passer createFromParcel(Parcel source) {
            return new Passer(source);
        }

        @Override
        public Passer[] newArray(int size) {
            return new Passer[size];
        }
    };
}
