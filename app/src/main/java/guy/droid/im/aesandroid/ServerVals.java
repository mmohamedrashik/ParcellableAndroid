package guy.droid.im.aesandroid;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by admin on 7/20/2017.
 */

public class ServerVals implements Parcelable {
    int id;
    String name;
    List<ServerValArray> serverValArrays;

    public ServerVals() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeTypedList(this.serverValArrays);
    }

    protected ServerVals(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.serverValArrays = in.createTypedArrayList(ServerValArray.CREATOR);
    }

    public static final Creator<ServerVals> CREATOR = new Creator<ServerVals>() {
        @Override
        public ServerVals createFromParcel(Parcel source) {
            return new ServerVals(source);
        }

        @Override
        public ServerVals[] newArray(int size) {
            return new ServerVals[size];
        }
    };
}
