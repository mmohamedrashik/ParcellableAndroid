package guy.droid.im.aesandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class GSONActivity extends AppCompatActivity {
    FrameLayout logger_view,recycler_view;
    List<ServerVals> serverVals;
    TextView generate_view;

    String display_text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gson);
        logger_view = (FrameLayout)findViewById(R.id.logger_view);
        recycler_view = (FrameLayout)findViewById(R.id.recycler_view);
        generate_view = (TextView)findViewById(R.id.generate_view);
        serverVals = new ArrayList<>();

        for(int i = 0;i<10;i++)
        {
            ServerVals serverVal = new ServerVals();
            List<ServerValArray> serverValArrays_pack = new ArrayList<>();
            for(int j =0;j<5;j++)
            {
                ServerValArray server_pack = new ServerValArray();
                server_pack.s_id = i+j;
                server_pack.sub_name = "sub-raz-"+i+"-"+j;
                serverValArrays_pack.add(server_pack);
            }
            serverVal.id = i;
            serverVal.name = "RAZ-K-"+i;
            serverVal.serverValArrays = serverValArrays_pack;
            serverVals.add(serverVal);
        }
        Gson gson = new Gson();
        display_text = gson.toJson(serverVals);

    }

    public void Generate(View view)
    {
        logger_view.setVisibility(View.VISIBLE);
        recycler_view.setVisibility(View.GONE);
        generate_view.setText(display_text);

    }

    public void Views(View view)
    {

        recycler_view.setVisibility(View.VISIBLE);
        logger_view.setVisibility(View.GONE);
    }
}
