package guy.droid.im.aesandroid;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PasserGetter extends AppCompatActivity {
    TextView passerview;
    SharedPreferences mPrefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_passer_getter);
        mPrefs  = getSharedPreferences("AESAPP", Context.MODE_PRIVATE);
        passerview = (TextView)findViewById(R.id.passer_message);

        Bundle extras = getIntent().getExtras();
        Passer passer = extras.getParcelable("parcelled");
        Log.d("PARSER",passer.id+" - "+passer.name);
        List<String> skills = new ArrayList<>();
        skills = passer.skills;
        for(int i =0;i<skills.size();i++)
        {
            passerview.append(skills.get(i)+"\n");
            Log.d("CRYPTOS",skills.get(i));
        }

        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String passer_String = gson.toJson(passer);
        Log.d("OBJECT_STRING",passer_String);
        prefsEditor.putString("PASSER", passer_String);
        prefsEditor.commit();



    }
}
