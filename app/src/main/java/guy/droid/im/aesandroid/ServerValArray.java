package guy.droid.im.aesandroid;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 7/20/2017.
 */

public class ServerValArray implements Parcelable {
    int s_id;
    String sub_name;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.s_id);
        dest.writeString(this.sub_name);
    }

    public ServerValArray() {
    }

    protected ServerValArray(Parcel in) {
        this.s_id = in.readInt();
        this.sub_name = in.readString();
    }

    public static final Parcelable.Creator<ServerValArray> CREATOR = new Parcelable.Creator<ServerValArray>() {
        @Override
        public ServerValArray createFromParcel(Parcel source) {
            return new ServerValArray(source);
        }

        @Override
        public ServerValArray[] newArray(int size) {
            return new ServerValArray[size];
        }
    };
}
