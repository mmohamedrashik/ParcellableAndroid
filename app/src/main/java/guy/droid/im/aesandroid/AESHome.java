package guy.droid.im.aesandroid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import java.security.Key;
import java.util.ArrayList;
import java.util.List;

public class AESHome extends AppCompatActivity {

    EditText user_data;
    TextView encryptm,decrypt;
    private static final String AES_MESSAGE = "AES";
    private static String algorithm = "AES";
    static byte[] keyValue = new byte[]
            { 'A','E','S','E','N','C','R','Y','P','T','I','O','N','R','A','Z' };
    String current_encstring = "";
    List<String> crypted;
    SharedPreferences mPrefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aeshome);
        user_data = (EditText)findViewById(R.id.aes_message);
        encryptm = (TextView)findViewById(R.id.enc_message);
        decrypt = (TextView)findViewById(R.id.dec_message);
        crypted = new ArrayList<>();
        mPrefs  = getSharedPreferences("AESAPP", Context.MODE_PRIVATE);
        String json = mPrefs.getString("PASSER", "");
        Log.d("JSON_",json);
        if(json.isEmpty())
        {

        }else
        {
            Gson gson = new Gson();
            Passer passer = gson.fromJson(json, Passer.class);
            for (int i =0;i<passer.skills.size();i++)
            {
                crypted.add(passer.skills.get(i));
            }

        }


    }

    public  void Encrypt(View view)
    {
        try {
            current_encstring = encrypt(user_data.getText().toString());
            crypted.add(current_encstring);
            encryptm.setText(current_encstring);
        } catch (Exception e) {
            Log.d(AES_MESSAGE,"ENC_ERROR : "+e.toString());
        }
    }
    public  void Decrypt(View view)
    {
        try {
            decrypt.setText(decrypt(current_encstring));
        } catch (Exception e) {
            Log.d(AES_MESSAGE,"DESYNC_ERROR : "+e.toString());
        }
    }

    public void GsonAct(View view)
    {
        startActivity(new Intent(getApplicationContext(),GSONActivity.class));
    }
    // Performs Encryption
    public static String encrypt(String plainText) throws Exception
    {
        Key key = generateKey();
        Cipher chiper = Cipher.getInstance(algorithm);
        chiper.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = chiper.doFinal(plainText.getBytes());
        String encryptedValue = Base64.encodeToString(encVal, Base64.DEFAULT);
      //  String encryptedValue = new BASE64Encoder().encode(encVal);
        return encryptedValue;
    }
    public static String decrypt(String encryptedText) throws Exception
    {
        // generate key
        Key key = generateKey();
        Cipher chiper = Cipher.getInstance(algorithm);
        chiper.init(Cipher.DECRYPT_MODE, key);
        byte[] decordedValue =  Base64.decode(encryptedText,Base64.DEFAULT);
       // byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedText);
        byte[] decValue = chiper.doFinal(decordedValue);
        String decryptedValue = new String(decValue);
        return decryptedValue;
    }
    private static Key generateKey() throws Exception
    {
        Key key = new SecretKeySpec(keyValue, algorithm);
        return key;
    }

    public void passer(View view)
    {

        Passer passer;
        String json = mPrefs.getString("PASSER", "");
        Log.d("JSON_",json);
        if(json.isEmpty())
        {
            passer =new Passer();
            Log.d("JSON_","EMPTY");
        }else
        {
            Gson gson = new Gson();
            passer = gson.fromJson(json, Passer.class);
            Log.d("JSON_","NON_EMPTY"+passer.skills.toString());

        }

      // Passer passer =new Passer();
        passer.id = 1;
        passer.name = "razz";
        passer.skills = crypted;
        Log.d("PARSER",passer.name);
        Intent intent =  new Intent(getApplicationContext(),PasserGetter.class);
        intent.putExtra("parcelled",passer);
        startActivity(intent);

    }
}
